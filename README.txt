FG YMML Custom Scenery
======================
for FlightGear, the open source flight simulator
www.flightgear.org

This package contains auto-generated OSM buidings for the YMML area. This is work in progress.

Thomas Albrecht
radi_@web.de
27 June 2015

Copyright (C) 2015 Thomas Albrecht

Install
-------

This package contains a standard FG_SCENERY directory structure. Assuming you've cloned it to

/home/user/custom_scenery_YMML

append that path to your FG_SCENERY variable (http://wiki.flightgear.org/$FG_SCENERY).

Then copy the fgdata/ directory to your FG_ROOT (http://wiki.flightgear.org/$FG_ROOT) to enable textures. 

Contributers and Acknowledgements
---------------------------------

Buildings and roads auto-generated from OpenStreetMap data using osm2city
http://wiki.flightgear.org/Osm2city.py
